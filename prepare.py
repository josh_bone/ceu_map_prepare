import json
import gspread
import os
from oauth2client.client import SignedJwtAssertionCredentials
from collections import OrderedDict

level_array = ['Oktober 6 u. 7 | 1',
               'Oktober 6 u. 7 | 2',
               'Nador u. 11 | 1',
               'Nador u. 11 | 2']

def main():
    """
    Author: Jozsef Gabor Bone <jozsef.bone@gmail.com>

    Map data prepration for CEU:
    - Google Authentication
    - Google Spreadsheet data download
    - Data transformation
    - JSON output
    """
    with open('init.json') as data_file:
        json_out = json.load(data_file, object_pairs_hook=OrderedDict)

    rowcount = 0
    json_key = json.load(open('CEU map prepare-4a2dc08bf43a.json'))
    scope = ['https://spreadsheets.google.com/feeds']

    credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key('1ZOjO0wBsX6vCshQNP0yoaCUNOCzJj8kXxUVqgYubhsI')
    worksheet = sh.worksheet("Actual import")

    data = worksheet.get_all_values()

    for row in data:
        if rowcount > 0:
            id = row[0]
            building = row[1]
            floor = row[2]
            room = row[3]
            category = row[4]
            unit = row[5]
            occupant = row[6]
            link = row[7]
            description = row[8]
            about = row[9]
            coord_x = row[10]
            coord_y = row[11]

            key = " | ".join((building, floor))
            level_index = find_element_in_list(key, level_array)

            if level_index >= 0:
                location = OrderedDict()
                location["id"] = id
                location["title"] = room
                location["category"] = category
                location["description"] = description
                location["about"] = about
                location["x"] = coord_x
                location["y"] = coord_y
                location["zoom"] = 2

                json_out["levels"][level_index]["locations"].append(location)

        rowcount += 1

    if os.path.exists('locations.json'):
        os.remove('locations.json')

    f = open('locations.json', 'w')
    f.write(json.dumps(json_out, indent=4))
    f.close()


def find_element_in_list(element, list_element):
    try:
        index_element=list_element.index(element)
        return index_element
    except ValueError:
        return -1

if __name__ == "__main__":
    main()